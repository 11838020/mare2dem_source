!-----------------------------------------------------------------------
!
!    Copyright 2021
!    Kerry Key
!    Lamont-Doherty Earth Observatory, Columbia University
!    kkey@ldeo.columbia.edu
!
!    This file is part of MARE2DEM.
!
!    MARE2DEM is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    MARE2DEM is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with MARE2DEM.  If not, see <http://www.gnu.org/licenses/>.
!
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------------------------------------------------------------------
    module mare2dem_lib_interface
!
! Interface for calling MARE2DEM from external codes for forward and inverse computations
!
! General sequence of calling from external routine:
!    (1) mare2dem_load_files      -  loads mare2dem format data and model files into Fortran memory, does not return arrays to Julia
!    (2) mare2dem_forward()       - computes forward response and misfit for freeResistivity array of free parameter vals
!          or
!        mare2dem_inversion()     - computes full inversion
!
! Only need to do step (1) once at the start. Then step (2) can be done any number of times for varying freeResistivity arrays.
!
! Optional:
!    mare2dem_get_params()        - can be called after (1) to get rho and y,z centroids of free parameters
!    mare2dem_get_data()          - can be called after (1) to get data and standard error arrays
!    mare2dem_save_resistivity()  - save free parameters into a mare2dem .resistivity file
!
!
! Caveats: this interface is not yet designed for general purpose usage. It has been coded for the minimal requirements for various
! research projects and thus has limited functionality.
!  mare2dem_forward() is meant to be called from an external inversion routine that only passes in free parameter resistivity
!  input array and returns only the resulting chi^2 misfit.
!
!-----------------------------------------------------------------------------------------------------------------------------------

    use occam
    use mare2dem_mpi                ! for MPI communicator tags etc
    use mare2dem_global           ! for setting defaults and storing worker status array
    use mare2dem_io
    use mare2dem_common
    use mare2dem_penaltymatrix

    use, intrinsic :: iso_fortran_env, only: int64, real64
    use, intrinsic :: iso_c_binding, only: c_char

    implicit none

    private

    public :: mare2dem_load_files, mare2dem_get_params, mare2dem_get_data, mare2dem_get_penaltymatrix
    public :: mare2dem_forward, mare2dem_inversion, mare2dem_save_resistivity
    public :: c_char_to_f_str, f_str_to_c_char

    external :: mkl_set_num_threads, insertFreeParams, displayJointInvMisfits

    contains

!-----------------------------------------------------------------------------------------------------------------------------------
!----------------------------------------------------------------------------------------------------------------mare2dem_load_files
!-----------------------------------------------------------------------------------------------------------------------------------
    subroutine mare2dem_load_files(n,inputResistivityFile,nFreeRegions,nParamsPerRegion,nData,nPen_nrows,nPen_nnz, bQuiet) &
                                 & bind(C, name="mare2dem_load_files")
!
! Loads the .resistivity, .poly, data and settings files into mare2dem memory. If inversion model, also generates the inversion's
! roughness penalty matrix.
!
! Returns number of free regions and the number of resistivity params per region
!  (so total free params = nFreeRegions*nParamsPerRegion)
!

    integer(int64),                 intent(in)  :: n
    character(len=1,kind=c_char),   intent(in)  :: inputResistivityFile(n)
    integer(int64),                 intent(out) :: nFreeRegions,nParamsPerRegion,nData,nPen_nrows,nPen_nnz
    integer(int64),                 intent(in)  :: bQuiet  ! if > 0, all print statements turned off

!
! Convert input c_char array to fortran character string:
!
    call c_char_to_f_str(n,inputResistivityFile,resistivityFile)

    if (bQuiet > 0) then
        call printNothing
    else
        call displayBanner
    endif

!
! Load MARE2DEM .resistivity, .poly, .settings and data files:
!
    call readFiles
    
    if ( nFree < 1000 ) lUseInversionMeshCoarsening = .false. ! don't use coarsening if small model

    if (nFree > 0) call generate_PenaltyMatrix(inputmodel)

    ! copy over to output variables:
    nParamsPerRegion = nRhoPerRegion
    nFreeRegions     = nFree/nRhoPerRegion
    nData            = nd
    nPen_nrows       = pen%nrows
    nPen_nnz         = pen%nnz

    end subroutine mare2dem_load_files

!-----------------------------------------------------------------------------------------------------------------------------------
!----------------------------------------------------------------------------------------------------------mare2dem_save_resistivity
!-----------------------------------------------------------------------------------------------------------------------------------
    subroutine mare2dem_save_resistivity(n,inputSaveFile,nLog10RhoFreeIn,misfit,iter) bind(C, name="mare2dem_save_resistivity")
!
! Loads the .resistivity, .poly, data and settings files into mare2dem memory
! Returns number of free regions and the number of resistivity params per region (so total free params = nFreeRegions*nParamsPerRegion)
!
    integer(int64),                 intent(in) :: n
    character(len=1,kind=c_char),   intent(in) :: inputSaveFile(n)
    real(real64), dimension(nFree), intent(in) :: nLog10RhoFreeIn
    real(real64),                   intent(in) :: misfit
    integer(Int64),                 intent(in) :: iter


    integer     :: niter_out
    external    :: insertFreeParams

!
! Convert input c_char array to fortran character string:
!
    call c_char_to_f_str(n,inputSaveFile,outputFileRoot)
     ! note outputFileRoot is global variable defined in maredem_common.f90 and used by subroutine writeResistivityFile() below

!
! Insert input log10(Rho) free parameters into rhoParams array (which stores free AND fixed parameters)
!
    call insertFreeParams(nLog10RhoFreeIn)  !  rhoParams is then used by writeResistivity below

!
! Save to file:
!
! note that this routine writes to a file named: <outputFileRoot>.<nCurrentIter>.resistivity and we're setting nCurrentIter=0 below

    niter_out = iter
    call writeResistivityFile(niter_out,misfit,0d0,0d0,0d0)
    ! writeResistivityFile(nCurrentIter,misfit,misfitRequested,lagrange,roughness)


    end subroutine mare2dem_save_resistivity

!-----------------------------------------------------------------------------------------------------------------------------------
!----------------------------------------------------------------------------------------------------------------mare2dem_get_params
!-----------------------------------------------------------------------------------------------------------------------------------
    subroutine mare2dem_get_params(nLog10RhoFree,freeCentroids,n)  bind(C, name="mare2dem_get_params")

    use kx_io, only: iFreeParam, rhoParams

    integer(int64), intent(in)                  :: n
    real(real64), dimension(n),     intent(out) :: nLog10RhoFree
    real(real64), dimension(n,2),   intent(out) :: freeCentroids

    integer :: i,j,ict

    ! check if
    if (n /= nFree) then
        write(*,*) 'Error in mare2dem_lib_interface subr. mare2dem_get_params(), input n /= nFree! '
        stop
    endif

!
! Copy free parameters to output:
!
    ict = 0
    do i = 1,nRegions
        do j = 1,nRhoPerRegion
            if (iFreeParam((i-1)*nRhoPerRegion+j)>0) then ! note this is NOT setup for cole-cole ip
                ict = ict + 1
                nLog10RhoFree(ict) = log10(rhoParams((i-1)*nRhoPerRegion+j))
            endif
        enddo
    enddo

!
! Get free region centroids:
!
    call getFreeRegionCentroids()
    freeCentroids = freeRegionCentroids


    end subroutine mare2dem_get_params

!-----------------------------------------------------------------------------------------------------------------------------------
!------------------------------------------------------------------------------------------------------------------mare2dem_get_data
!-----------------------------------------------------------------------------------------------------------------------------------
    subroutine mare2dem_get_data(nData,nUncertainty,n)  bind(C, name="mare2dem_get_data")

    integer(int64), intent(in)                  :: n
    real(real64), dimension(n),     intent(out) :: nData(n)
    real(real64), dimension(n,2),   intent(out) :: nUncertainty(n)

    ! check if
    if (n /= nd) then
        write(*,*) 'Error in mare2dem_lib_interface subr. mare2dem_get_data(), input n /= nd! '
        stop
    endif

    !
    ! Copy data to output:
    !
    nData = d
    nUncertainty  = sd

    end subroutine mare2dem_get_data

!-----------------------------------------------------------------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------------------mare2dem_get_penaltymatrix
!-----------------------------------------------------------------------------------------------------------------------------------
    subroutine mare2dem_get_penaltymatrix(row,col,val,nnz)  bind(C, name="mare2dem_get_penaltymatrix")

    integer(int64),                     intent(in)  :: nnz
    integer(int64), dimension(nnz),     intent(out) :: row(nnz),col(nnz)
    real(real64),   dimension(nnz),     intent(out) :: val(nnz)

    integer :: i,j

    ! check if
    if (nnz /= pen%nnz) then
        write(*,*) 'Error in mare2dem_lib_interface subr. mare2dem_get_penaltymatrix(), input nnz /= pen%nnz! '
        stop
    endif

    !
    ! Copy data to output:
    !
    do i = 1,pen%nrows
        do j = pen%rowptr(i),pen%rowptr(i+1)-1
            row(j) = i
            col(j) = pen%colind(j)
            val(j) = pen%val(j)
        enddo
    enddo

    end subroutine mare2dem_get_penaltymatrix

!-----------------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
    subroutine mare2dem_forward(nproc_per_team,nLog10RhoFreeIn,chiSquaredMisfit, &
                               & n,inputSaveFile,saveNum,nResp,bQuiet) &
                               & bind(C, name="mare2dem_forward")
!
! Routine for computing MARE2DEM forward response that can be called from external libraries.
! You must first call mare2dem_load_files(inputResistivityFile) on the rank 0 processor
!
! Inputs:   nproc_per_team   variable to use to create local mpi communicator in Fortran in same way as done in
!                            external calling code, which may be running multiple forward models in parallel
!                            on separate teams of processors. e.g.,Julia  PT chains running using nproc_per_team cores
!                            for each PT chain. The calling routine needs to have teams set up identically
!                            (we recreate the MPI communicator here so its available to mare2dem's fortran routines)
!           nLog10RhoFreeIn  1D array (size nFree) of free parameter log10 resistivities (log10 ohm-m)
!
! Outputs:  chiSquaredMisfit data fit to input model using nRhoFreeIn
!

    integer(int64),                 intent(in)   :: nproc_per_team
    real(real64), dimension(nFree), intent(in)   :: nLog10RhoFreeIn
    real(real64),                   intent(out)  :: chiSquaredMisfit
    integer(int64),                 intent(in)   :: n
    character(len=1,kind=c_char),   intent(in)   :: inputSaveFile(n)
    integer(int64),                 intent(in)   :: saveNum       ! if > 0, then forward response is saved to a file
    real(real64), dimension(nd),    intent(out)  :: nResp           ! response array
    integer(int64),                 intent(in)   :: bQuiet        ! if > 0, all print statements turned off

    integer  :: rank,team_rank,nproc,team,mcomm_size, ierr

    external :: mkl_set_num_threads, computeFwd

!
! Intel MKL specific: make sure one thread per MPI process so mkl solver calls don't oversubscribe
!
    call mkl_set_num_threads ( 1 )

!
! Get rank and setup communicator team(s)
!
    call mpi_comm_rank( MPI_COMM_WORLD, rank, ierr )
    call mpi_comm_size( MPI_COMM_WORLD, nproc, ierr )

    if ( (nproc_per_team == 1) .or. (nproc == 2) ) then
        write(*,*) ' '
        write(*,*) '!!!!! Error: MARE2DEM needs to be run using at least 2 processors !!!'
        write(*,*) '             You specified only one, sorry, stopping!'
        write(*,*) ' '
        call exitMARE2DEM()
    endif
    nworkers = nproc_per_team - 1

    team   = int(rank/nproc_per_team)

    call mpi_comm_split(MPI_COMM_WORLD, team, rank, mcomm ,ierr) ! assigns this rank to be part of mcomm for team.
    call mpi_comm_size(mcomm,mcomm_size,ierr)                    ! so each team has different mcomm
    call mpi_comm_rank(mcomm,team_rank,ierr)

    call mpi_barrier(mcomm,ierr)


    !
    ! Launch manager and worker controllers:
    !
    if (team_rank /= manager) then ! worker

        call mpi_worker_controller

    else ! manager

        if (bQuiet > 0) call printNothing

        !
        ! Initialize the worker status array:
        !
        allocate (lWorker_status(nworkers))
        lWorker_status  = .true.


        !
        ! Convert input c_char array to fortran character string:
        !
        call c_char_to_f_str(n,inputSaveFile,outputFileRoot)
        ! note outputFileRoot is global variable defined in maredem_common.f90 and used by subroutine writeResistivityFile() below


        !
        ! Compute forward response:
        !
        linversion = .false. ! tell mare2dem to NOT compute Jacobian matrix otherwise it'll run way more slowly
        call computeFwd( linversion, nLog10RhoFreeIn )

        deallocate(lWorker_status)

        !
        ! Compute chi-squared misfit
        !
        chiSquaredMisfit =  sum( ((d - dm)/sd)**2 )


        !
        ! Save response
        !
        if (saveNum > 0) then
            currentIteration = saveNum
            call writeResponse(currentIteration,dm)
        endif

        ! Copy response to output variable
        nResp  = dm

        !
        ! Tell workers to exit mpi_worker_controller:
        !
        call mpi_shutDownWorkers()

    endif

    !
    ! Broadcast chiSquaredMisfit to all processors on local communicator
    !
    call mpi_bcast(chiSquaredMisfit, 1, MPI_DOUBLE_PRECISION, manager, mcomm, ierr)



    call mpi_barrier(mcomm,ierr)

    call mpi_comm_free(mcomm,ierr)

    end subroutine mare2dem_forward

!-----------------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
    subroutine mare2dem_inversion(inv_method,nproc_per_team,log10Mu,&
                                 & nLog10RhoFreeIO,nLog10RhoPrej, &
                                 & nData, nResp, &
                                 & nStartingIteration, &
                                 & n,inputSaveFile,iSaveLogfile,iSaveModel,iSaveResp, bQuiet, &
                                 & chiSquaredMisfit, convergenceFlagOut, &
                                 & niterations, totaltime) &
                                 & bind(C, name="mare2dem_inversion")
!
! Routine for running a MARE2DEM inversion by calling from an external routine, possible in another coding language.
! You must first call mare2dem_load_files(inputResistivityFile) on the rank 0 processor
!
    use mare2dem_input_data_params

    ! input variables:

    integer(int64),                 intent(in)    :: inv_method      ! 0 = occam, 1 = static mu gauss-newton
    integer(int64),                 intent(in)    :: nproc_per_team  ! size of local mpi communicator team for this inversion call
    real(real64),                   intent(in)    :: log10Mu         ! log10 lagrange multiplier to start occam with or use for fix mu
    real(real64), dimension(nFree), intent(inout) :: nLog10RhoFreeIO ! input: starting resistivity, output: final model
    real(real64), dimension(nFree), intent(in)    :: nLog10RhoPrej   ! prejudice (aka prior) model log10 resistivity
    real(real64), dimension(nd),    intent(in)    :: nData           ! data array
    real(real64), dimension(nd),    intent(out)   :: nResp           ! response array
    integer(int64),                 intent(in)    :: nStartingIteration  ! iteration number of input model, used for output name only
    integer(int64),                 intent(in)    :: n
    character(len=1,kind=c_char),   intent(in)    :: inputSaveFile(n)
    integer(int64),                 intent(in)    :: iSaveLogfile    ! 1 = save inversion log file
    integer(int64),                 intent(in)    :: iSaveModel      ! 1 = save final model to file, 2 = save all iterations
    integer(int64),                 intent(in)    :: iSaveResp       ! 1 = save final response to file, 2 = save all iterations
    integer(int64),                 intent(in)    :: bQuiet          ! > 0 turns off all print statements
    ! iSave = 0 means no files are output and results only returned through variable passing to calling code

    ! output variables:
    real(real64),                   intent(out)  :: chiSquaredMisfit   ! chi^2 misfit
    integer(int64),                 intent(out)  :: convergenceFlagOut ! see below
    integer(int64),                 intent(out)  :: niterations       ! number of iterations
    real(real64),                   intent(out)  :: totaltime         !  s

    ! Static Mu:
    !  2 - target misfit obtained
    !  4 - can't reduce misfit
    !  6 - max # iterations
    ! Occam inversion:
    !  0 - Successful iteration: Model RMS was lowered but target RMS has not been obtained
    !  1 - Successful iteration: Target RMS has been obtained and Occam smoothing is in progress
    !  2 - Normal convergence:   Target RMS has been obtained, model has been smoothed until the step size is below a threshold
    !  3 - Unusual convergence:  A perfectly smooth model has been found. Occam is done.
    !  4 - Convergence problem:  Occam could not find a model with an RMS less than the last iteration
    !  5 - Convergence problem:  Occam found the target misfit, but the model roughness couldn't be decreased.
    !  6 - Convergence problem:  Maximum number of iterations achieved.


    ! local variables:
    integer             :: rank,team_rank,nproc,team,mcomm_size, ierr, iWorker
    logical             :: lrunMARE2DEM
    logical             :: lSaveJacobian
    logical             :: lSaveSensitivity
    real(8)             :: timeOffset, time0
!
! Intel MKL specific: make sure one thread per MPI process so mkl solver calls don't oversubscribe
!
    call mkl_set_num_threads ( 1 )

!
! Start the timer:
!
    call get_time_offset(0d0, time0)

!
! Get rank and setup communicator team(s)
!
    call mpi_comm_rank( MPI_COMM_WORLD, rank, ierr )
    call mpi_comm_size( MPI_COMM_WORLD, nproc, ierr )

    if ( (nproc_per_team == 1) .or. (nproc == 2) ) then
        write(*,*) ' '
        write(*,*) '!!!!! Error: MARE2DEM needs to be run using at least 2 processors !!!'
        write(*,*) '             You specified only one, sorry, stopping!'
        write(*,*) ' '
        call exitMARE2DEM()
    endif
    nworkers = nproc_per_team - 1

    team   = int(rank/nproc_per_team)

    call mpi_comm_split(MPI_COMM_WORLD, team, rank, mcomm ,ierr) ! assigns this rank to be part of mcomm for team.
    call mpi_comm_size(mcomm,mcomm_size,ierr)                    ! so each team has different mcomm
    call mpi_comm_rank(mcomm,team_rank,ierr)

    call mpi_barrier(mcomm,ierr)

    lSaveSensitivity = .false.
    lSaveJacobian    = .false.

    !
    ! Launch manager and worker controllers:
    !
    if (team_rank /= manager) then ! worker

        call mpi_worker_controller

        ! After workers done with tasks, don't forget to dealloc these arrays created during mpi_all_Bcast() 
        if (allocated(prewts))     deallocate(prewts,pen%colind,pen%val,pen%rowptr)
        if (allocated(preDiffwts)) deallocate(preDiffwts,ijDiff)

    else ! manager

        !
        ! Copy over input nData into occam module's d:
        !

        if ( (allocated(d)) ) then
            if ( (size(d) == size(nData)) ) then
                d = nData
            else
                write(*,*) 'Error in mare2dem_inversion! input data not same size as data array loaded by mare2dem_load_files(). '
                stop
            endif
        else
            write(*,*) 'Error in mare2dem_inversion! data array has not been previously loaded by call to mare2dem_load_files()'
            stop
        endif

        !
        ! Turn off print statements if requested:
        !
        occamPrintLevel  = 1      ! 0: print nothing, 1: print logfile statements to the terminal window.

        if (bQuiet > 0) then
            call printNothing
            occamPrintLevel = 0
        endif

        !
        ! Initialize the worker status array:
        !
        allocate (lWorker_status(nworkers))
        lWorker_status  = .true.

        !
        ! Broadcast regularization arrays to workers for parallel inversion matrix construction and storage:
        !
        do iWorker = 1,nworkers
            call mpi_send( iflag_TellWorkerToBcast, 1, MPI_INTEGER, iWorker, tag_M_SendingData, mcomm, ierr )
        enddo
        call mpi_all_Bcast


        !
        ! Initialize the scratch folders
        !
        if ((nTxCSEM > 0).or.(nTxDC>0)) call mpi_manager_initializeScratch

        currentIteration = nStartingIteration

        ! If saving response for every iteration, also save the normalized sensitivity matrix:
         if ( iSaveResp == 2) lSaveSensitivity = .true.

        !
        ! Convert input c_char array to fortran character string:
        !
        call c_char_to_f_str(n,inputSaveFile,outputFileRoot)
        ! note outputFileRoot is global variable defined in maredem_common.f90 and used by subroutine writeResistivityFile() below

        !
        ! Open a logfile:
        !
        if (iSaveLogfile==1) call openOccamLogFile(outputFileRoot)

        !
        ! Initialize the inversion settings:
        !
        lrunMARE2DEM       = .true.

        ! Copy over input parameters into occam module variables:
        modelLog10Mu = log10Mu
        pm      = nLog10RhoFreeIO
        premod  = nLog10RhoPrej

        ! if premod nonzero, set prewts to 1 so premod is used in Occam
        if (any(premod /= 0. )) then
            prewts = 1.
            lrough_with_prej = .true. ! assume this is a special call...
        else
            lrough_with_prej = .false.
            prewts = 0.
        endif

        call transformToUnbound_inputarrays() ! transforms pm and premod into unbound arrays using lowerBound,upperBound & lBoundMe

        !
        ! Run the main loop
        !

        do while (lrunMARE2DEM)

            currentIteration = currentIteration + 1

            !
            ! Compute an Occam iteration:
            !
            select case (inv_method)
                case (0) ! occam:
                    call computeOccamIteration(lSaveJacobian,lSaveSensitivity)
                case (1)  ! static mu gauss-newton:
                    call computeStaticMuIteration(lSaveJacobian,lSaveSensitivity)
            end select

            !
            ! Write out the results:
            !
            if ( (iSaveResp == 2) .and. (currentIteration==1) ) call writeResponse(0,dm_0)   ! save input starting model response  to <>.0.resp


            if ( convergenceFlag /= 4 .and. convergenceFlag /= 5 ) then

                ! Convert pm from unbound to bound space and insert into nRhoParams:
                call insertFreeParams(transformToBound(pm,lowerBound,upperBound,lBoundMe))
                ! in em2d.f90, inserts pm into rhoParams array used by writeResistivityFile
                ! we also transform from Occam's unbound space to real resistivity in bound space
                ! since pm is always in unbounded space

                ! Display any joint inversion misfits:
                call displayJointInvMisfits()

                ! output an iteration file of current rhoParams array:
                if (iSaveModel == 2) call writeResistivityFile(currentIteration,modelRMS,targetRMS,modelLog10Mu,modelRoughness)
                ! note that these are all shared from occam module: modelRMS,modelLog10Mu,modelRoughness

                ! output the model response:
                if ( iSaveResp == 2) call writeResponse(currentIteration,dm)  ! response of model from by this iteration

            endif

            !
            ! Check the convergence flag:
            !
                if (convergenceFlag > 1)  exit ! convergenceFlag = 0 when target RMS is not yet met and inversion is still descending misfit
            
        enddo

        ! Close the Occam log file:
        if (iSaveLogfile==1) call closeOccamLogFile

        deallocate(lWorker_status)

        ! Convert pm from unbound to bound space and insert into nRhoParams:
        call insertFreeParams(transformToBound(pm,lowerBound,upperBound,lBoundMe))
        ! in em2d.f90, inserts pm into rhoParams array used by writeResistivityFile
        ! we also transform from Occam's unbound space to real resistivity in bound space
        ! since pm is always in unbounded space

        !
        ! Save final inverted response and model if requested:
        !
        !if ( convergenceFlag /= 4  ) then ! save final model for all convergenceFlags except 4 (when rms did not decrease)

            ! output an iteration file of final rhoParams array:
            if (iSaveModel == 1) call writeResistivityFile(currentIteration,modelRMS,targetRMS,modelLog10Mu,modelRoughness)
            ! note that these are all shared from occam module: modelRMS,modelLog10Mu,modelRoughness

            ! output the model response:
            if ( iSaveResp == 1)  call writeResponse(currentIteration,dm)

        !endif

        call get_time_offset( time0,timeOffset)

        ! copy over to output variables:
        totaltime          = timeOffset
        niterations        = currentIteration
        convergenceFlagOut = convergenceFlag
        chiSquaredMisfit   = sum( ((d - dm)/sd)**2 )
        nResp              = dm
        nLog10RhoFreeIO    = transformToBound(pm,lowerBound,upperBound,lBoundMe)


        !
        ! Tell workers to exit mpi_worker_controller:
        !
        call mpi_shutDownWorkers()

    endif ! manager


    call mpi_barrier(mcomm,ierr)

    call mpi_comm_free(mcomm,ierr)


    end subroutine mare2dem_inversion
!-----------------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
    subroutine c_char_to_f_str(n,c_str,f_str) ! result()

    integer(int64),                 intent(in) :: n
    character(len=1,kind=c_char),   intent(in) :: c_str(n)

    character(256),   intent(out)   :: f_str

    integer         :: i

    f_str = ''
    !write(*,*) 'n: ',n
    do i = 1,n
        !write(*,*) i, c_str(i:i)
        f_str(i:i) = c_str(i)
    enddo

    end subroutine

!-----------------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
!-----------------------------------------------------------------------------------------------------------------------------------
    subroutine f_str_to_c_char(f_str,n,c_str)

    character(256),                 intent(in)  :: f_str
    integer(int64),                 intent(out) :: n
    character(len=1,kind=c_char),   intent(out) :: c_str(*)


    integer :: i

    n = len_trim(f_str)
    !write(*,*) ' f to c, n: ',n
    do i = 1,n
        c_str(i) = f_str(i:i)
        !write(*,*) i, c_str(i)
    enddo

    end subroutine

end module mare2dem_lib_interface
